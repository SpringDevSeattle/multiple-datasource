package com.company.foo.web.controller;

import com.company.foo.repository.DataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class StatusController
{


    @Autowired
    DataRepository dataRepository;


    @RequestMapping(value = "/status", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String getStatus()
    {
        String s1 = dataRepository.testPriTemplate();
        String s2 = dataRepository.testSecTemplate();

        if (s1.equalsIgnoreCase("success") && s2.equalsIgnoreCase("success")){
            return "success";
        }

        else
            return "failed";
    }
}

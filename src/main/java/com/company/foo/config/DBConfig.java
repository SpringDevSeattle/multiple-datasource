package com.company.foo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;



@Configuration
public class DBConfig {


    @Bean(name="priDataSource")
    @Primary
    @ConfigurationProperties(prefix="spring.datasource")
    public DataSource priDataSource() {
        return DataSourceBuilder.create().build();
    }


    @Bean(name = "secDataSource")
    @ConfigurationProperties(prefix="spring.sec.datasource")
    public DataSource secDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "jdbcPriTemplate")
    @Autowired
    public JdbcTemplate jdbcPriTemplate(@Qualifier("priDataSource") DataSource hostds) {
        return new JdbcTemplate(hostds);
    }

    @Bean(name = "jdbcSecTemplate")
    @Autowired
    public JdbcTemplate jdbcSecTemplate(@Qualifier("secDataSource") DataSource secDataSource) {
        return new JdbcTemplate(secDataSource);
    }

}
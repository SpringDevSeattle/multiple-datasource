package com.company.foo.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;


@Repository
public class DataRepository {

    @Autowired
    @Qualifier("jdbcPriTemplate")
    private JdbcTemplate jdbcPriTemplate;

    @Autowired
    @Qualifier("jdbcSecTemplate")
    private JdbcTemplate jdbcSecTemplate;


    public String testPriTemplate(){
        jdbcPriTemplate.execute("select 1");
        return "success";
    }

    public String testSecTemplate(){
        jdbcSecTemplate.execute("select 1");
        return "success";
    }
}
